import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";

import App from "./App";

const countReducer = (state = 0, action) => {
	console.log(`Received ${action.type} dispatch in countReducer`);

	if (action.type === "ADD_COUNT") {
		return action.payload;
	}
	if (action.type === "DECREAMENT_COUNT") {
		return state - 1;
	}
	return state;
};

const weightReducer = (state = 185, action) => {
	console.log(`Received ${action.type} dispatch in weightReducer`);
	return state;
};

const HeightReducer = (state = 200, action) => {
	console.log(`Received ${action.type} dispatch in HeightReducer`);
	return state;
};

const reducer = combineReducers({
	count: countReducer,
	weight: weightReducer,
	height: HeightReducer,
});

const store = createStore(reducer);
console.log("Store is", store.getState());

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("root")
);
