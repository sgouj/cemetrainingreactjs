export const addCount = (count) => {
	return {
		type: "ADD_COUNT",
		payload: count,
	};
};

export const decreamentCount = (count) => {
	return {
		type: "DECREAMENT_COUNT",
	};
};
