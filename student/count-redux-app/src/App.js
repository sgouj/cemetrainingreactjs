import React from "react";
import { connect } from "react-redux";
import { addCount, decreamentCount } from "./Actions";
import Height from "./Height";

const App = (props) => {
	console.log("App props", props);
	return (
		<React.Fragment>
			<div>App Application</div>
			<p>
				Count:{props.count}{" "}
				<button
					onClick={() => {
						props.addCount(props.count + 1); // we need to dispatch this action
					}}
				>
					Add COunt
				</button>
				<button
					onClick={() => {
						props.decreamentCount(); // we need to dispatch this action
					}}
				>
					Decreament Count
				</button>
			</p>
			<p>Weight:{props.weight}</p>
			<p>
				<Height name={"sandeep"} />
			</p>
		</React.Fragment>
	);
};

const mapStateToProps = (state) => {
	console.log("state is", state);
	return {
		count: state.count,
		weight: state.weight,
	};
};

const actionCreators = {
	addCount,
	decreamentCount,
};

export default connect(mapStateToProps, { addCount, decreamentCount })(App); //returns app component with the props that we want
