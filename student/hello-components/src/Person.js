import React from "react";
import Greets from "./Greet";

class Person extends React.Component {
	constructor(props) {
		super(props);
		//set initial state
		this.state = { height: 185, weight: 200 };

		// this.grow = this.grow.bind(this);
	}

	componentDidMount() {
		console.log("It renderd ");
	}

	growHeight = () => {
		this.setState((state) => {
			return {
				height: state.height + 1,
			};
		});
		this.setState((state) => {
			return {
				height: state.height + 1,
			};
		});
		this.setState((state) => {
			return {
				height: state.height + 1,
			};
		});
	};

	growWeight = () => {
		this.setState({ weight: this.state.weight + 1 });
	};

	render() {
		return (
			<div>
				<Greets person={this.props.name} name="Allstate" />

				<p>
					{this.props.name} weight {this.state.weight} lbs and
					{this.state.height}
					cm
				</p>
				<button onClick={this.growWeight}>Grow Weight</button>
				<button onClick={this.growHeight}>Grow height</button>
			</div>
		);
	}
}

export default Person;
