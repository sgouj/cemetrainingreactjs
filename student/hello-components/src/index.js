import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import Person from "./Person";
import Greets from "./Greet";

const PersonFunc = (props) => {
	const [height, setHeight] = useState(185);
	const [weight, setWeight] = useState(200);

	useEffect(() => {
		console.log("It also rendered");
	}, []); //like component

	useEffect(() => {
		console.log("Weight and Height changed");
	}, [height, weight]); // on value change of height and weight

	return (
		<div>
			<Greets person={props.name} name="Allstate" />

			<p>
				{props.name} weight {weight} lbs and {height}
				cm
			</p>
			<button onClick={() => setWeight(weight + 1)}>Grow Weight</button>
			<button onClick={() => setHeight(height + 1)}>Grow height</button>
		</div>
	);
};

const main = (
	<React.Fragment>
		<Person name="John" />
		<PersonFunc name="Sandeep" />
	</React.Fragment>
);

ReactDOM.render(main, document.querySelector("#root"));
