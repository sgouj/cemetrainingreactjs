import React from "react";

class Greets extends React.Component {
	constructor(props) {
		console.log();
		super(props);
	}
	render() {
		return (
			<h1>
				{this.props.person} from {this.props.name}
			</h1>
		);
	}
}

export default Greets;

//this is functional components that receives properties as props

// function Greet(name, person) {
// 	// console.log(props);
// 	return (
// 		<h1>
// 			Hello {name} from {person}
// 		</h1>
// 	);
// }

// function Greet(props) {
// 	// console.log(props);
// 	return (
// 		<React.Fragment>
// 			<h1>
// 				Hello {props.name} from {props.person}
// 			</h1>
// 			<Greeting />
// 		</React.Fragment>
// 	);
// }

// const Greeting = () => {
// 	return <h1>Arrow Function</h1>;
// };

// class Greets extends React.Component {
// 	constructor(props) {
// 		console.log();
// 		super(props);
// 	}
// 	render() {
// 		return <h1>Class - Component welcome {this.props.person}</h1>;
// 	}
// }
