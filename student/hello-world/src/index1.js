import React from "react";
import ReactDOM from "react-dom";

import "./index.css";

// const header = React.createElement("h1", null, "My header");

// const text = React.createElement(
// 	"p",
// 	{ className: "hello-text", id: "hello-id" },
// 	"Hello World!"
// );

// const main = React.createElement("div", { className: "hello-container" }, [
// 	header,
// 	text,
// ]);

// This is done using React.createElement
// const main = React.createElement("div", { id: "ice-cream" }, [
// 	React.createElement("h1", null, "Ice Cream Flavours"),
// 	React.createElement("ul", { class: "ice-cream-list" }, [
// 		React.createElement("li", null, "Vanilla"),
// 		React.createElement("li", null, "Chocolate"),
// 		React.createElement("li", null, "Raspberry Ripple"),
// 	]),
// ]);

// this is doing using JSX (Javascript XML)
const shopName = "AllState Ice Cream Shop";

const chocoStyle = { color: "brown" };

function getFlavours() {
	return (
		<React.Fragment>
			<h2>Ice Cream Flavours</h2>
			<ul className="ice-cream-list">
				<li>Vanilla</li>
				<li style={chocoStyle}>Chocolate</li>
				<li className="raspberry-color">Raspberry Ripple</li>
				<li className="cookies-color">Cookies & Cream</li>
			</ul>
		</React.Fragment>
	);
}

const main = (
	<section data-shop="AllState">
		<div>
			<h1>{shopName}</h1>
		</div>
		<div id="ice-cream">{getFlavours()}</div>
	</section>
);

ReactDOM.render(main, document.getElementById("root"));
