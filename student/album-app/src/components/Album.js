import React, { useState } from "react";

import AlbumTitle from "./AlbumTitle";
import AlbumArtist from "./AlbumArtist";
import AlbumTracks from "./AlbumTracks";
import ShowHideButton from "./ShowHideButton";

const Album = (props) => {
	const [visible, setVisibity] = useState(true);
	return (
		<div className="col-md-4">
			<div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
				<div className="card-body">
					<AlbumTitle title={props.album.title} />
					<AlbumArtist artist={props.album.artist} />
					<AlbumTracks tracks={props.album.tracks} visible={visible} />
					<ShowHideButton toggle={setVisibity} visible={visible} />
				</div>
			</div>
		</div>
	);
};

export default Album;
