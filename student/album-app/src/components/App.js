import React, { useState, useEffect } from "react";
import axios from "axios";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import "./App.css";
import Banner from "./Banner";
import Album from "./Album";
import AddAlbumPage from "./AddAlbumPage";

const App = () => {
	const [albums, setAlbums] = useState([]);
	const [refresh, setRefresh] = useState(false);
	//Same as componentDidMount
	useEffect(() => {
		console.log("App is mounted");
		axios.get("http://localhost:8080/albums").then((res) => {
			console.log(res);
			setAlbums(res.data);
		});
	}, [refresh]); // if album change state, then call this function again

	return (
		<Router>
			<div className="app">
				<nav>
					<ul>
						<li to="">
							<Link to="/">Home</Link>
						</li>
						<li>
							<Link to="/add">Add Album</Link>
						</li>
					</ul>
				</nav>
				<Banner />
				<Switch>
					<Route exact path="/add">
						<AddAlbumPage refresh={refresh} setRefresh={setRefresh} />
					</Route>
					<Route path="/">
						<div className="container">
							<div className="row">
								{albums.map((album) => (
									<Album key={album.title} album={album} />
								))}
							</div>
						</div>
					</Route>
				</Switch>
			</div>
		</Router>
	);
};

export default App;

// const albums = [
// 	{
// 		title: "Album 1",
// 		artist: "Artist 1",
// 		tracks: ["Track 1", "Track 2", "Track 3"],
// 	},
// 	{
// 		title: "Album 2",
// 		artist: "Artist 2",
// 		tracks: ["Track 4", "Track 5", "Track 6"],
// 	},
// 	{
// 		title: "Album 3",
// 		artist: "Artist 3",
// 		tracks: ["Track 6", "Track 7", "Track 9"],
// 	},
// 	{
// 		title: "Album 4",
// 		artist: "Artist 4",
// 		tracks: ["Track 1", "Track 2", "Track 3"],
// 	},
// 	{
// 		title: "Album 5",
// 		artist: "Artist 5",
// 		tracks: ["Track 4", "Track 5", "Track 6"],
// 	},
// 	{
// 		title: "Album 6",
// 		artist: "Artist 6",
// 		tracks: ["Track 6", "Track 7", "Track 9"],
// 	},
// ];
