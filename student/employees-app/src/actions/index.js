import axios from "axios";

export const fetchEmployeeBegin = () => {
	return {
		type: "FETCH_EMPLOYEES_BEGIN",
	};
};

export const fetchEmployeeSuccess = (employees) => {
	return {
		type: "FETCH_EMPLOYEES_SUCCESS",
		payload: employees,
	};
};

export const fetchEmployeesFailure = (err) => {
	return {
		type: "FETCH_EMPLOYEES_FAILURE",
		payload: { message: "Failed to fetch employees.. please try again later" },
	};
};

export const fetchEmployee = () => {
	return (dispatch, getState) => {
		dispatch(fetchEmployeeBegin());
		axios.get("http://localhost:8080/api/all").then(
			(res) => {
				console.log("Get Employee", res.data);
				dispatch(fetchEmployeeSuccess(res.data));
			},
			(err) => {
				dispatch(fetchEmployeesFailure(err));
			}
		);
	};
};
