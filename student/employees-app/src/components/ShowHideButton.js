import React from "react";

const ShowHideButton = (props) => {
	return (
		<React.Fragment>
			<button
				className="btn btn-sm btn-outline-secondary"
				onClick={() => props.setVisibleDetails(!props.visibleDetails)}
			>
				View details »
			</button>
			{/* <p>
					<a class="btn btn-secondary" href="#" role="button">
						View details »
					</a>
				</p> */}
		</React.Fragment>
	);
};

export default ShowHideButton;
