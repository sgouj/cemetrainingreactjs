import React, { useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

const AddEmployee = ({ refresh, setRefresh }) => {
	const history = useHistory();
	const [id, setId] = useState("");
	const [name, setName] = useState("");
	const [salary, setSalary] = useState("");
	const [age, setAge] = useState("");
	const entities = useSelector((state) => state.entities);
	console.log(entities);

	const callApiSave = () => {
		axios
			.post("http://localhost:8080/api/save", {
				id: id,
				name: name,
				salary: salary,
				age: age,
			})
			.then(() => {
				console.log("Employee Added");
				setRefresh(!refresh);
				history.push("/");
			});
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		console.log("submit form clicked");
		callApiSave();
	};

	return (
		<div className="container">
			<h3>Add New Employee</h3>
			<form onSubmit={handleSubmit}>
				<div className="form-row">
					<div className="form group col-md-5">
						<label>ID:</label>
						<input
							id="title"
							type="text"
							value={id}
							onChange={(e) => setId(e.target.value)}
						/>
					</div>
					<div className="form group col-md-5">
						<label>Name:</label>
						<input
							id="artist"
							type="text"
							value={name}
							onChange={(e) => setName(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form group col-md-5">
						<label>Salary:</label>
						<input
							id="salary"
							type="number"
							value={salary}
							onChange={(e) => setSalary(e.target.value)}
						/>
					</div>
					<div className="form group col-md-5">
						<label>Age:</label>
						<input
							id="age"
							type="number"
							value={age}
							onChange={(e) => setAge(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<input type="submit" value="Create Album" />
				</div>
			</form>
		</div>
	);
};

export default AddEmployee;
