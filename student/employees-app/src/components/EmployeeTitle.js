import React from "react";

const EmployeeTitle = (props) => {
	return (
		<React.Fragment>
			<h2>{props.name}</h2>
		</React.Fragment>
	);
};

export default EmployeeTitle;
