import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import MenuAppBar from "../components/shared/MenuAppBar";
import Breadcrumb from "../components/shared/Breadcrumb";
import Card from "../components/shared/Card";
import { fetchEmployee } from "../actions";
import PageNotFound from "../components/shared/PageNotFound";

const App = (props) => {
	//	const [employees, setEmployees] = useState([]);
	const dispatch = useDispatch();
	const [refresh, setRefresh] = useState(false);

	const entities = useSelector((state) => state.entities);

	useEffect(() => {
		dispatch(fetchEmployee());
	}, []);

	console.log("entities", entities);
	return (
		<Router>
			<React.Fragment>
				{/* <Banner /> */}
				<MenuAppBar />
				{/* <Breadcrumb /> */}
				<Switch>
					<Route exact path="/" component={Card}></Route>
					<Route component={PageNotFound} />
				</Switch>
				{/* <Card /> */}
				{/* <ul>
					<li>
						<Link to="/">Home</Link>
					</li>
					<li>
						<Link to="/addEmployee">Add Employee</Link>
					</li>
				</ul> */}
				{/* <Switch>
					<Route path="/addEmployee">
						<AddEmployee refresh={refresh} setRefresh={setRefresh} />
					</Route>
					<Route exact path="/">
						<div className="container">
							<div className="row">
								{entities.map((employee) => (
									<Employee key={employee.id} employee={employee} />
								))}
							</div>
						</div>
					</Route>
				</Switch> */}
			</React.Fragment>
		</Router>
	);
};

export default App;

// const employees = [
//     {
//         id: 1,
//         name: "Employee1",
//         age: 10,
//         salary: 1000,
//         email: "arpi@abc.com",
//     },
//     {
//         id: 2,
//         name: "Employee 2",
//         age: 20,
//         salary: 2000,
//         email: "arpi@abc.com",
//     },
//     {
//         id: 3,
//         name: "Employee 3",
//         age: 30,
//         salary: 3000,
//         email: "Anjan@abc.com",
//     },
//     {
//         id: 4,
//         name: "Employee 4",
//         age: 40,
//         salary: 4000,
//         email: "sandeep@abc.com",
//     },
// ];
