import React, { useState } from "react";
import EmployeeTitle from "./EmployeeTitle";
import EmployeeDetails from "./EmployeeDetails";
import ShowHideButton from "./ShowHideButton";

const Employee = (props) => {
	const [visibleDetails, setVisibleDetails] = useState(false);
	return (
		<React.Fragment>
			<div className="col-md-4">
				<div className="card mb-4" style={{ width: "18rem" }}>
					<div className="card-body">
						<EmployeeTitle name={props.employee.name} />
						<EmployeeDetails
							employee={props.employee}
							visibleDetails={visibleDetails}
						/>
						<ShowHideButton
							visibleDetails={visibleDetails}
							setVisibleDetails={setVisibleDetails}
						/>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};

export default Employee;
