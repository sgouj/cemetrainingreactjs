import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import HomePage from "../../assets/images/HomePage.svg";
import AddEmployee from "../AddEmployee";
import Employee from "../Employee";

const Card = () => {
	const [refresh, setRefresh] = useState(false);

	const entities = useSelector((state) => state.entities);

	return (
		<Router>
			<React.Fragment>
				<div className="container" style={{ marginTop: "18px" }}>
					<div className="row">
						<Link to="/home" className="col col-md-3">
							<div className="card h-100">
								<p className="card-body">
									<img
										src={HomePage}
										className="bd-placeholder-img card-img-top"
									/>
									HOME
								</p>
							</div>
						</Link>
						<Link to="/add_employee" className="col col-md-3">
							<div className="card h-100">
								<p className="card-body">
									<img
										src={HomePage}
										className="bd-placeholder-img card-img-top"
									/>
									ADD EMPLOYEE
								</p>
							</div>
						</Link>
					</div>
				</div>
				<Switch>
					<Route exact path="/home">
						<div className="container">
							<div className="row">
								{entities.map((employee) => (
									<Employee key={employee.id} employee={employee} />
								))}
							</div>
						</div>
					</Route>
					<Route exact path="/add_employee">
						<AddEmployee refresh={refresh} setRefresh={setRefresh} />
					</Route>
				</Switch>
			</React.Fragment>
		</Router>
	);
};

export default Card;
