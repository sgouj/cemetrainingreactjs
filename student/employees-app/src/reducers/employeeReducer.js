const initialState = { entities: [] };

const employeeReducer = (state = initialState, action) => {
	console.log(`received ${action.type} dispatch in Employee`);
	switch (action.type) {
		case "FETCH_EMPLOYEES_SUCCESS":
			console.log("Reducer State", action.payload);
			return {
				...state,
				entities: action.payload,
				loading: false,
			};

		default: {
			return state;
		}
	}
};

export default employeeReducer;
